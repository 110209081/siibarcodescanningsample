package com.seikoinstruments.barcodescansample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class LicenseInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_open_source_license);
    }
}
