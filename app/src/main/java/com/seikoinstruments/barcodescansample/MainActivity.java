package com.seikoinstruments.barcodescansample;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seikoinstruments.sdk.mobileprinter.PrinterException;
import com.seikoinstruments.sdk.mobileprinter.PrinterInterface;
import com.seikoinstruments.sdk.mobileprinter.PrinterManager;
import com.seikoinstruments.sdk.mobileprinter.PrinterModel;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
    private static PrinterManager mPrinterManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPrinterManager = new PrinterManager(getApplicationContext());
    }

    /**
     * Load camera and scan barcode.
     */
    public void onScanBarcodeBtnClick(View view) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(ScanActivity.class);
        integrator.setPrompt(getString(R.string .scan_activity_text1));
        integrator.setOrientationLocked(false);
        integrator.setBeepEnabled(true);
        integrator.initiateScan();
    }

    /**
     * Handle scan result.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result == null) {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }
        if(result.getContents() == null) {
            Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();

        // Generate Bluetooth address from scan result
        String address = getAddressFromScanResult(result.getContents());
        if(address.isEmpty()) {
            // Invalid address
            showMessageDialog(getString(R.string .connection_failed),"Scanned: " + result.getContents() + getString(R.string .print_exception_invalid_address));
            return;
        }

        // Connect to printer and print test
        connectPrinterAndPrintTest(address);
    }

    /**
     * Generate Bluetooth address from scan result.
     */
    private String getAddressFromScanResult(String address) {
        if(!isMACAddress(address)) {
            return "";
        }

        // Convert Bluetooth address format: AABBCCDDEEFF -> AA:BB:CC:DD:EE:FF
        StringBuilder sb = new StringBuilder(address);
        for (int i = 2; i < sb.length() ; i += 3) {
            sb = sb.insert (i, ":");
        }
        return sb.toString();
    }

    /**
     * Connect printer and print test.
     */
    private void connectPrinterAndPrintTest(String address) {
        String message = getString(R.string .print_text_bluetooth_address) + address;
        try {
            // Starts using a printer
            mPrinterManager.open(PrinterInterface.PRN_IF_BT, PrinterModel.PRN_MODEL_MP_A40, address, 0, true);

            // Check printer status
            if (!mPrinterManager.getStatus().getErrOffline()
                    && !mPrinterManager.getStatus().getErrOutOfPaper()
                    && !mPrinterManager.getStatus().getErrCoverOpen()) {
                // Test Print
                mPrinterManager.printText(getString(R.string .connection_succeeded) + message + "\n\n\n");
            }

            // Show a success message
            showMessageDialog(getString(R.string .connection_succeeded) , message);

        } catch (PrinterException e) {
            showMessageDialog(getString(R.string .connection_failed) , message + getString(R.string .print_exception_errorcode) + e.getErrorCode().toString() );
        } catch(Exception ex){
            showMessageDialog(getString(R.string .connection_failed), ex.getMessage());
        } finally {
            if (mPrinterManager != null && mPrinterManager.isOpened()) {
                try {
                    // Close printer
                    mPrinterManager.close();
                } catch (PrinterException e) {
                    showMessageDialog(getString(R.string .connection_failed), message + getString(R.string .print_exception_errorcode) + e.getErrorCode().toString());
                }
            }
        }
    }

    /**
     * Create option menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.information, menu);
        return true;
    }

    /**
     * Handle option item selection.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if(item.getItemId() == R.id.menu_open_source_license){
            openSourceLicenseInfo();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Load LicenseInfoActivity.
     */
    public void openSourceLicenseInfo(){
        //To license information activity
        startActivity(new Intent(getApplication(), LicenseInfoActivity.class));
    }

    /**
     * Show message dialog.
     */
    private void showMessageDialog(String ret, String msg) {
        new AlertDialog.Builder(this)
                .setTitle(ret)
                .setMessage("\r\n" + msg)
                .setPositiveButton("OK", null)
                .show();
    }

    /**
     * Validates a MAC address.
     */
    public static Boolean isMACAddress(String macAddress) {
        //Regular Expression
        return Pattern.compile("^[0-9A-Fa-f]{12}$").matcher(macAddress).find();
    }
}
